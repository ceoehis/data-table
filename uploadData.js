const csvToJson = require('csvtojson');
const csvFilePath = './payments_data.csv';
const firebase = require('firebase/app');
require('firebase/firestore');
const config = require('./src/config/config');

const db = firebase.initializeApp(config).firestore();

csvToJson({
  noheader: false,
  headers: ['id', 'name', 'description', 'date', 'amount'],
})
  .fromFile(csvFilePath)
  .then(payments => {
    console.log(payments);
    payments.forEach(payment => {
      db.collection('payments')
        .add(payment)
        .then(function(docRef) {
          console.log('Document written with ID: ', docRef.id);
        })
        .catch(function(error) {
          console.error('Error adding document: ', error);
        });
    });
  });
