import paymentService from '../../services/paymentService';
import NProgress from 'nprogress';

export const state = {
  loading: false,
  payments: [],
  perPage: 10,
  paymentsCount: 0,
};

export const mutations = {
  LOADING(state, isLoading) {
    state.loading = isLoading;
  },

  SET_PAYMENTS(state, payments) {
    state.payments = payments;
  },

  SET_PAYMENTS_COUNT(state, paymentsCount) {
    state.paymentsCount = paymentsCount;
  },
};

export const actions = {
  getAllPayments({ commit, state }, { page }) {
    NProgress.start();
    commit('LOADING', true);
    return paymentService.getPayments(state.perPage, page).then(res => {
      commit('LOADING', false);
      commit('SET_PAYMENTS', res.data);
      commit('SET_PAYMENTS_COUNT', parseInt(res.headers['x-total-count']));
      NProgress.done();
    });
  },
};
