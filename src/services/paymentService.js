import axios from 'axios';

const apiClient = axios.create({
  baseURL: `http://localhost:3000`,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 10000,
});

export default {
  /**
   * what I woudl want to do would be to dynamically search and sort the array of values
   *
   * also the searech field shoule trigger a debounced request to fetch a subset of payment results;
   */
  getPayments(perPage, page) {
    return apiClient.get(
      '/payments?_limit=' +
        perPage +
        '&_page=' +
        page +
        '&_sort=Date&_order=desc'
    );
  },
};
